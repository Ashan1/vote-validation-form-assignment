<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Vote Validation System</title>
    <link rel="stylesheet" href="css/style.css">
    
    <style type="text/css">
    
    .pic{

    animation-name: rotates;
    animation-duration: 8s;
    animation-timing-function: linear;
    animation-iteration-count: infinite;
    
}

@keyframes rotates{

    from{ 
        transform: rotateY(0deg)
    }
    to{
        transform: rotateY(360deg)
    }
}
    
    </style>
    
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
        <a class="navbar-brand" href="#">Vote Validation System</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
            aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>

            </ul>

        </div>
    </nav>

    <div class="card mb-3">
        <h3 class="card-header">User Validator</h3>
        <div class="card-body">
            <h5 class="card-title">Rules and Regulations</h5>
            <h6 class="card-subtitle text-muted">To be a valid voter</h6>
        </div>
        <img style="height: 275px; width: 100%; display: block;" src="images/daterrminovae-ludzi.jpg" alt="Card image">
        <div class="card-body">
            <p class="card-text">The rules and regulations are under the Democratic Socialist and Republic Sri Lanka
            </p>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">Should be more than 18 years</li>
            <li class="list-group-item">Should be a Sri Lankan</li>
            <li class="list-group-item">Should not be a multicitizen</li>
        </ul>

        <div class="card-footer text-muted">
            Get validated
        </div>
    </div>
    <div class="jumbotron">
        <h1 class="display-3">Vote Validation Form</h1>
        <hr class="my-4">



        <form action="FormInput" method="get">
            <fieldset>

                <div class="form-group">
                    <label for="exampleInputEmail1">First Name</label>
                    <input type="text" class="form-control" id="fname" aria-describedby="emailHelp"
                        placeholder="Enter first name" name="firstName" required>

                </div>

                <div class="form-group">
                    <label for="exampleInputEmail2">Last Name</label>
                    <input type="text" class="form-control" id="lname" aria-describedby="emailHelp"
                        placeholder="Enter last name" name="lastName" required>

                </div>

                <div class="form-group">
                    <label for="exampleInputEmail3">Age</label>
                    <input type="number" class="form-control" id="age" aria-describedby="emailHelp"
                        placeholder="Enter your age" name="age" required>

                </div>

                <div class="form-group">
                    <label for="exampleSelect1">Example select</label>
                    <select class="form-control combo" id="exampleSelect1" name="citizen">
                        <option value="srilankan">SriLankan</option>
                        <option value="other">Other</option>
                    </select>
                </div>


                <fieldset class="form-group">
                    <legend>Citizenships</legend>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="citizens" value="true">
                            I have multiple Citizenships
                        </label>
                    </div>


                </fieldset>

                <button type="submit" class="btn btn-outline-primary">submit</button>

            </fieldset>



        </form>
    </div>

    <div class="card1 mb-3">
        <h3 class="card-header">Sri Lanka</h3>
        <div class="card-body">
            <h5 class="card-title">Spread peace and harmony</h5>
            <h6 class="card-subtitle text-muted">Justice everywhere</h6>
        </div>
        <img class="pic" style="height: 250px; width: 100%; display: block;" src="images/pic1.jpg" alt="Card image">
        <div class="card-body">
            <p class="card-text c1">The rules and regulations are under the Democratic Socialist and Republic Sri Lanka
            </p>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">Should be more than 18 years</li>
            <li class="list-group-item">Should be a Sri Lankan</li>
            <li class="list-group-item">Should not be a multicitizen</li>
        </ul>

        <div class="card-footer text-muted">
            Get validated
        </div>
    </div>
</body>

</html>