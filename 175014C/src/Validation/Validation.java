package Validation;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import View.Citizen;

public class Validation {

	HttpServletRequest request;
	HttpServletResponse response;
	
	
	public Validation(HttpServletRequest request, HttpServletResponse response) throws IOException {
	
		this.request=request;
		this.response=response;
		
		
	}

	

	public void validate(Citizen citizen) throws ServletException, IOException {
		
		if(citizen.getAge()<19) {
			
			showMessage(citizen,"You are under-aged");
		//	System.out.println("you are under age.you cant vite");
			
		}else if(citizen.getCitizen().equals("other")) {
			
			showMessage(citizen,"You are not a Sri Lankan");
		//	System.out.println("you are foreigner.you cant vite");
			
			
			
		}else if(citizen.isCitizens()) {
			
			showMessage(citizen,"You have multiple citizenships");
		//	System.out.println("you have multiple citizens.you cant vite");
			
			
			//response.sendRedirect("/Form.jsp");
		}else {

			
			//Kasun Lakmal is valid user for vote in Srilanka.
			
			
		 	request.setAttribute("firstName", citizen.getFirstName());
		 	request.setAttribute("lastName", citizen.getLastName());
		 	request.setAttribute("age",citizen.getAge());
		 	request.getRequestDispatcher("/Result.jsp").forward(request, response);
			
		}
		
		
		
		
	}

	private void showMessage(Citizen citizen,String msg) throws IOException, ServletException {
		
		request.setAttribute("firstName", citizen.getFirstName());
	 	request.setAttribute("lastName", citizen.getLastName());
	 	request.setAttribute("msg",msg);
	 	request.getRequestDispatcher("/alert.jsp").forward(request, response);
		
		
	}
	
	
	
}
