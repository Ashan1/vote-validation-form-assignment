<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>


<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width: 80%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0} 
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}


.material-icons{

    animation-name: scales;
    animation-duration: 0.5s;
    animation-timing-function: linear;
    animation-iteration-count: infinite;
   
}

@keyframes scales{

    from{ 
        transform: scale(1,1)
    }
    to{
        transform: scale(1.5,1.5)
    }

}

.redirect{

color:white;
text-decoration: none;
padding:5px;
font-size: 15px;
border-radius: 4px;

}
.redirect:hover{

background-color: white;
color:#f77676;

}

/* The Close Button */
.close {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.modal-header {
  padding: 2px 16px;
  background-color: #f77676;
  color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
  padding: 2px 16px;
  background-color: #f77676;
  color: white;
}
</style>
</head>
<body>

<h2></h2>

<!-- Trigger/Open The Modal -->
<button id="myBtn"></button>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header">
      <span class="close">&times;</span>
      <h2>! Warning !</h2>
      
    </div>
    <div class="modal-body">
      <h4>Dear ${firstName} ${lastName},</h4>
      <p>${msg}</p>
       <i class="material-icons" style="font-size:48px;margin-left:650px;margin-top:-100px;color:red">error</i>
      <h5>You cannot Vote ....!</h5>
     
      
    </div>
    <div class="modal-footer">
   <h2><a class="redirect" href="Form.jsp">Redirect</a></h2> 
    </div>
  </div>


</div>

<script src="js/alert.js">
// Get the modal

</script>

</body>
</html>
