

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Validation.Validation;
import View.Citizen;

/**
 * Servlet implementation class FormInput
 */
@WebServlet("/FormInput")
public class FormInput extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	Validation validation;
	
    public FormInput() {
        
    	
       
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		
		getInput(request,response);
		
		
	}

	private void getInput(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Citizen citizen = new Citizen();
		
		
		String firstname = request.getParameter("firstName");
		String lastname = request.getParameter("lastName");
		int age = Integer.parseInt(request.getParameter("age"));
		String Citizen = request.getParameter("citizen");
		boolean multipleCitizen = Boolean.parseBoolean(request.getParameter("citizens"));
		System.out.println("This is Servlet 2");
		System.out.println(firstname+"\n"+lastname+"\n"+age+"\n"+Citizen+"\n"+multipleCitizen);
		
		
		citizen.setFirstName(firstname);
		citizen.setLastName(lastname);
		citizen.setAge(age);
		citizen.setCitizen(Citizen);
		citizen.setCitizens(multipleCitizen);
		
		this.validation = new Validation(request,response);
		this.validation.validate(citizen);
		
		
		
		
		
	}

	


	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
